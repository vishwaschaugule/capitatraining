using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Author:Vishwas Chaugule
/// Date:19/09/2018
/// </summary>
namespace Capita.UI.ConsoleUi
{
    /// <summary>
    /// Delegate declaration
    /// </summary>
    /// <param name="clientName"></param>
    public delegate void OutSource(string clientName);
    /// <summary>
    /// Event Generator/Publisher Class
    /// </summary>
    public class EventOrganiser
    {
        /// <summary>
        /// Declaring event based on delegate
        /// </summary>
        public event OutSource triggerEvent;
        /// <summary>
        /// Raising Event based on delegate object
        /// </summary>
        /// <param name="cName"></param>
        public void RaiseEvent(string cName)
        {
            if (triggerEvent != null)
            {
                triggerEvent(cName);
            }
        }

    }

    /// <summary>
    /// service class
    /// </summary>
    public class BdayPartyVendor
    {
        /// <summary>
        /// Service method
        /// </summary>
        /// <param name="clientName"></param>
        public void DJService(string clientName)
        {
            Console.WriteLine("AAj ke Party " + clientName + "Ke Taraf Se! ...Coming SOON Aishwarya's Party");
        }
    }

    /// <summary>
    /// service class
    /// </summary>
    public class BreakUpPartyPlanner
    {
        /// <summary>
        /// service method
        /// </summary>
        /// <param name="clientName"></param>
        public void TravellingService(string clientName)
        {
            Console.WriteLine(clientName + " Going to Heaven!");
        }
    }
    /// <summary>
    /// subcriber class
    /// </summary>
    class DelegateDemo
    {
        public static void Main()//
        {
            ///Object Creation
            EventOrganiser paritosh = new EventOrganiser();
            EventOrganiser rahul = new EventOrganiser();
            BdayPartyVendor vishwas = new BdayPartyVendor();
            BreakUpPartyPlanner aishwarya = new BreakUpPartyPlanner();

            
            //Event Raising based on delegate object reference
            //multicast delegate
            paritosh.triggerEvent += new OutSource(vishwas.DJService);
            paritosh.triggerEvent += new OutSource(aishwarya.TravellingService);
            rahul.triggerEvent += new OutSource(vishwas.DJService);
            rahul.triggerEvent += new OutSource(aishwarya.TravellingService);
            paritosh.RaiseEvent("Pratik");
            rahul.RaiseEvent("Abinash");
            Console.ReadLine();

        }
    }

}
