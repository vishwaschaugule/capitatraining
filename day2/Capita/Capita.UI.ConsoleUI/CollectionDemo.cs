using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

/// <summary>
/// Author:Vishwas Chaugule
/// Date:19/09/2018
/// </summary>
namespace Capita.UI.ConsoleUi
{
    /// <summary>
    /// Non-Generic Collection Demo class
    /// </summary>
    class CollectionDemo
    {
        /// <summary>
        /// ArrayList Collection
        /// </summary>
        public static void Main()
        {
            
            ArrayList list1 = new ArrayList();
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(3.5f);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(3.5f);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add("Hello");
            foreach (dynamic item1 in list1)
            {
                Console.WriteLine(item1);
            }
            
            Console.WriteLine(list1.Count);//size of ArrayList
            Console.WriteLine(list1.Capacity);//Capacity of ArrayList,ArrayList doubles the Capacity when arrayList Capacity gets full  
            Console.ReadLine();
        }
    }
}
