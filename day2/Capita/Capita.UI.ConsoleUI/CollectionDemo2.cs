using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.UI.ConsoleUi
{
    class CollectionDemo2
    {
        static void Main(string[] args)
        {using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Author:Vishwas Chaugule
/// Date:19/09/2018
/// </summary>
namespace Capita.UI.ConsoleUi
{
    /// <summary>
    /// SortedList Collection Demo 
    /// </summary>
    class CollectionDemo2
    {
        static void Main(string[] args)
        {
            SortedList sl = new SortedList();

            ///Key-value Pair
            ///sorted by keys
            sl.Add("001", "Zara Ali");
            sl.Add("002", "Abida Rehman");
            sl.Add("003", "Joe Holzner");
            sl.Add("004", "Mausam Benazir Nur");
            sl.Add("005", "M. Amlan");
            sl.Add("006", "M. Arif");
            sl.Add("007", "Ritesh Saikia");

            if (sl.ContainsValue("Nuha Ali"))
            {
                Console.WriteLine("This student name is already in the list");
            }
            else
            {
                sl.Add("008", "Nuha Ali");
            }

            // get a collection of the keys. 
            ICollection key = sl.Keys;
            ///Access by Keys
            foreach (string k in key)
            {
                Console.WriteLine("Key:" + k + ": Value:" + sl[k]);
            }

            ICollection val = sl.Values;
            ///Access values 
            foreach (string k in val)
            {
                Console.WriteLine(k);
            }

            Console.ReadLine();
        }

    }
}
}

    }
}
