using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Author: Vishwas Chaugule
/// Date:19/09/2018
/// </summary>
namespace Capita.UI.ConsoleUi
{    
    /// <summary>
     ///Generic Class 
     /// </summary>
     /// <typeparam name="T"></typeparam>
    class Alter<T>// where T:class
    {
        /// <summary>
        /// Generic Method
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        public void Swap(ref T num1,ref T  num2)
        {
            T temp = num1;
            num1 = num2;
            num2 = temp;
        }
    }
    class GenericDemo
    {
        public static void Main()
        {
            int num1 = 5;
            int num2 = 3;
            string str1 = "Vishwas";
            string str2 = "Chaugule";

            //Swap Method calling with int parameter
            Alter<int> alt = new Alter<int>();
            Console.WriteLine("Before Swapping Number1: " + num1 + "Number2:  " + num2);
            alt.Swap(ref num1, ref num2);
            Console.WriteLine("After Swapping Number1: " + num1 + "Number2:  " + num2);

            //Swap Method calling with string parameter
            Alter<string> alt1 = new Alter<string>();
            Console.WriteLine("Before Swapping FirstName:  " + str1 + " LastName:  " + str2);
            alt1.Swap(ref str1, ref str2);
            Console.WriteLine("After Swapping FirstName:  " + str1+ " LastName:  " + str2);
            Console.ReadLine();
        }
    }
}
