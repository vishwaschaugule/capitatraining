﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Data.Entity
{
   public class Client
    {
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string Contact{ get; set; }
        public string Password { get; set; }
        public string Email{ get; set; }
    }
}
