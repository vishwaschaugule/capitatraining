﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Data.Entity
{
    public class Position
    {
        public string PositionId { get; set; }
        public string Description { get; set; }
        public string Role { get; set; }
        public string Band { get; set; }
        public string Designation { get; set; }
        public string Skill { get; set; }
    }
}
