﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Data.Entity
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAddress { get; set; }
        public string ContactNumber { get; set; }
        public string AaadhaarCard { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string PreviousOrganisation { get; set; }
        public string Designation { get; set; }
        public int Duration { get; set; }

    }
}
