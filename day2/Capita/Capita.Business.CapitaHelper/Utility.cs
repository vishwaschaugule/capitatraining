﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Author:Vishwas
/// Date: 19/09/2018
/// </summary>
namespace Capita.Business.CapitaHelper
{
    /// <summary>
    /// Helper class for Capita
    /// </summary>
        public class Utility
        {
            /// <summary>
            /// To perform addition 
            /// </summary>
            /// <param name="a">for 1st user input</param>
            /// <param name="b">for 2nd user input</param>
            /// <returns>returns addition</returns>
            public int Sum(int a, int b)
            {
                return a + b;
            }
        public int Sub(int a, int b)
        {
            return a - b;
        }
        public int Mult(int a, int b)
        {
            return a * b;
        }
        public int Div(int a, int b)
        {
            return a / b;
        }
        /// <summary>
        ///Pass by value 
        /// </summary>
        /// <param name="val"></param>

        public void Change1(int val)
            {
                val = 200;
                Console.WriteLine("In By value  function " + val.ToString());
            }
        /// <summary>
        /// Pass by reference
        /// </summary>
        /// <param name="val"></param>
                    
            public void Change2(ref int val)
            {
                val = 300;  
                Console.WriteLine("In By refrence  function " + val.ToString());
            }
        /// <summary>
        /// Out function
        /// </summary>
        /// <param name="val"></param>
            public void Change3(out int val)
            {
                val = 400;
                Console.WriteLine("In By out  function " + val.ToString());
            }
        /// <summary>
        /// Params Function
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
            public int Sum(params int[] values)
            {
                int result = 0;
                foreach (int item in values)
                {
                    result += item;
                }
                return result;
            }
            /// <summary>
            /// Optional parameter function
            /// </summary>
            /// <param name="values">Enter any Name</param>
            /// <returns></returns>
            public void Print(string firstName, string lastName = "Patil")
            {
                Console.WriteLine("First Name " + firstName + " Last Name " + lastName);
            }

        }
    }
