﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Capita.Business.CapitaHelper;
namespace Capita.UI.ConsoleUI
{
    class ParameterDemo
    {
        public static void Main()
        {
            Utility utility = new Utility();
            int myVal = 10;
            Console.WriteLine("Before calling by value function= {0}", myVal);//10
            utility.Change1(myVal);//200
            Console.WriteLine("After calling by value function= {0}", myVal);//10

            Console.WriteLine("\nBefore calling by reference function= {0}", myVal);//10
            utility.Change2(ref myVal);//300
            Console.WriteLine("After calling by reference function= {0}", myVal);//300

            Console.WriteLine("\nBefore calling by Out function= {0}", myVal);//300
            utility.Change3(out myVal);//400
            Console.WriteLine("After calling by out function= {0}", myVal);//400

            Console.WriteLine("\ncalling params function= {0}", myVal);//300
            myVal = utility.Sum(10, 20, 30);
            Console.WriteLine("After calling by out function= {0}", myVal);//60

            Console.WriteLine("\ncalling Optional function");
            utility.Print("Vishwas");

            Console.WriteLine("\ncalling Named function");
            utility.Print(lastName: "Chaugule", firstName: "Vishwas");
            Console.ReadLine();
        }
    }
}
